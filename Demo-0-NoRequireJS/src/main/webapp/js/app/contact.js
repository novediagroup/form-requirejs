function prepareMsg_(msg) {
	var nbs = unescape("%A0");
	var lines = [0];
	var max = 0;
	var i = 0;
	var last = i;
	for( var j = 1; true; j++ ) {
		last = i;
		i = msg.indexOf('\n',i+1);
	    if( i > 0 ) {
	    	lines[j] = i - last;
	    	if( lines[j] > max )
	    		max = lines[j];
		} else
			break;
	}
	last = msg.length - last;
	if( last > max )
		max = last;
	if( max < 20 )
		max = 20;
	msg = msg.replace(/ /g,nbs);
	for( var j = 1; j < lines.length; j++ )//multiplication because 'spaces' are thinner than other chars
		msg = msg.replace(/\n/,new Array(Math.round((max-lines[j])*1.8)).join(nbs)+' ');
  
	//fill also the last line so it won't be small enough to displayed together the previous
	return msg += new Array(Math.round((max-last)*1.8)).join(nbs);
}

$(document).ready(function() {
	
	// LocalScroll - "To the Top" links
	$(".toTheTop").localScroll();
    	
    // Cycle - Used for phone slideshow at the top of the page
	$("#slideshow").cycle({
		fx: 'fade',
		speed:  1000,
		timeout:  3500
	});
		
	// Clear Form Fields		
	$("input, textarea").searchField();
	
	// Jquery Form
	$('#contactForm').ajaxForm({ 
	    url:        '/contact', 
	    type:		'post',
	    beforeSubmit: function(arr, $form, options) { 
	    	var errorMsg = "";
	    	$.each(arr, function(index, input) {
	    		if(this.name === this.value) {
	    			errorMsg += "The field '"+this.name+"' must be filled !\n";
	    		}
	    	});
	    	if(errorMsg !== "") {
	    		$().toastmessage('showToast', {
		    		text     : prepareMsg_(errorMsg.substring(0, errorMsg.length - 2)),
		    		stayTime : 2000,
		    	    sticky   : false,
		    	    position : 'middle-center',
		    	    type     : 'error'
		    	});
	    		return false;
	    	}
	    },
	    success: function() {
	    	$().toastmessage('showToast', {
	    		text     : 'Your message has been successfully sent !',
	    		stayTime : 1800,
	    	    sticky   : false,
	    	    position : 'middle-center',
	    	    type     : 'success'
	    	});
	    }
	});
	
	// ToolTipster => tooltips
	$('.tooltip').tooltipster({
		position: 'bottom'
	});
});
