<%@ taglib prefix="spring"	uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" 		uri="http://java.sun.com/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<title><spring:message code="contact.title"/></title>

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="<c:url value="/css/screen.css"/>" media="all" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/css/tooltipster.css"/>" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/js/lib/fancybox/jquery.fancybox.css"/>" media="all" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/js/lib/toastmessages/css/jquery.toastmessage.css"/>" media="all" />

		<!-- JavaScript -->
		<script src="<c:url value="/js/lib/jquery-1.9.1.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.easing.1.3.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.localscroll-1.2.7-min.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.scrollTo-min.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.jcarousel.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.cycle.min.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/fancybox/jquery.fancybox.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.searchField.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.tooltipster.js"/>" type="text/javascript"></script>
		
		<script src="<c:url value="/js/lib/toastmessages/jquery.toastmessage.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.form.js"/>" type="text/javascript"></script>
		
		<script src="<c:url value="/js/app/contact.js"/>" type="text/javascript"></script>
	</head>
<body>
	<div id="content">
		<div class="copy">
			<div id="contact" class="clearfix">
				<div class="leftColumn">
					<h1 class="mainTitle"><spring:message code="contact.title"/></h1>
					
					<form id="contactForm" action="">
						<div class="left">
							<span class="input"><input type="text" title="<spring:message code="contact.form.name"/>" name="name" id="contactFormName" /></span>
							<span class="input"><input type="text" title="<spring:message code="contact.form.email"/>" name="e-mail" id="contactFormEmail" /></span>
							<span class="input"><input type="text" title="<spring:message code="contact.form.subject"/>" name="subject" id="contactFormSubject" /></span>
						</div>
						<div class="right">
							<span class="textarea"><textarea title="<spring:message code="contact.form.message"/>" name="message" rows="10" cols="10"></textarea></span>
							<button><spring:message code="contact.form.send"/></button>
						</div>
					</form>
				</div>
				<div class="rightColumn">
					<h2 class="mainTitle">Contact Details</h2>
					<ul id="contactDetails">
						<li id="contactName">Novedia Group</li>
						<li id="contactAddress">94/96 rue de Paris - 92100 Boulogne Billancourt</li>
						<li id="contactEmail">contact@novediagroup.com</li>
						<li id="contactPhone">+33 1 41 22 13 00</li>
						<li id="contactChat">novediagroup@skype.com</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
