<%@ taglib prefix="spring"	uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" 		uri="http://java.sun.com/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<title><spring:message code="about.title"/></title>

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="<c:url value="/css/screen.css"/>" media="all" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/css/tooltipster.css"/>" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/js/lib/fancybox/jquery.fancybox.css"/>" media="all" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/css/app/about.css"/>" />

		<!-- JavaScript -->
		<script src="<c:url value="/js/lib/jquery-1.9.1.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.easing.1.3.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.localscroll-1.2.7-min.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.scrollTo-min.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.jcarousel.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.cycle.min.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/fancybox/jquery.fancybox.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.searchField.js"/>" type="text/javascript"></script>
		<script src="<c:url value="/js/lib/jquery.tooltipster.js"/>" type="text/javascript"></script>
		
		<script src="<c:url value="/js/app/about.js"/>" type="text/javascript"></script>
	</head>
<body>
	<div id="content">
		<div class="copy">
			<div id="about" class="contentDivide clearfix">
				<div class="leftColumn">
					<h1 class="mainTitle"><spring:message code="about.title"/></h1>
					<p id="profile">Lorem ipsum dolor sit amet, adipiscing elit. Morbi, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio bonle cheos sanvw lorem non turpis. Nullam sit amet enim suspendisse naessdt id velit vitae ligula.</p>
					<h2 class="iconDesign"><spring:message code="about.mainblocs.interfacedesign"/></h2>
					<p>Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Morbi, ipsum sed pharetra gravida, orci magna rhoncus neque. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Morbi, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio bonle cheos sanvw lorem non turpis.</p>
					<h2 class="iconDevelopment"><spring:message code="about.mainblocs.appdev"/></h2>
					<p>Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Morbi, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio bonle cheos sanvw lorem non turpis.</p>
					<h2 class="iconBranding"><spring:message code="about.mainblocs.branding"/></h2>
					<p>Aliquam in lacus. Quisque pellentesque. , orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Morbi, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio bonle cheos sanvw lorem non turpis. Aliquam in lacus. Quisque pellentesque. , orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Morbi, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio bonle cheos sanvw lorem non turpis.</p>
				</div><!-- end leftColumn -->
				<div class="rightColumn">
					<h2 class="mainTitle"><spring:message code="about.mainblocs.skillset"/></h2>
						<ul id="skillset">
							<li class="star-5"><spring:message code="about.mainblocs.skillset.uidesign"/></li>
							<li class="star-4"><spring:message code="about.mainblocs.skillset.customicons"/></li>
							<li class="star-3"><spring:message code="about.mainblocs.skillset.branding"/></li>
							<li class="star-4"><spring:message code="about.mainblocs.skillset.mobileapps"/></li>
							<li class="star-2"><spring:message code="about.mainblocs.skillset.webdev"/></li>
						</ul>
					<h2 id="testimonialNav" class="mainTitle clearfix">
						<strong><spring:message code="about.mainblocs.testimonials"/></strong>
						<span>
							<a href="javascript:void(0);" id="prevTestimonials" class="disabled"><spring:message code="common.previous"/></a>
							<a href="javascript:void(0);" id="nextTestimonials"><spring:message code="common.next"/></a>
						</span>
					</h2>
						<div id="testimonials">
							<ul>
								<li class="first">
									<blockquote>Aliquam aliquet, est a ullamcorper condimentum, tellus nulla fringilla elit, a iaculis nulla turpis sed wisi. Fusce volutpat. Etiam sodales ante id nunc. Proin ornare dignissim lacus. Nunc porttitor nunc a sem. Sed sollicitudin velit eu magna. Aliquam erat volutpat. Vivamus ornare est non wisi. Proin vel quam. Vivamus egestas. Nunc tempor diam vehicula mauris. Nullam sapien eros, facilisis vel, eleifend non, auctor dapibus, pede.</blockquote>
									<cite>- John Doe, <a href="" title="">Company Name</a></cite>
								</li>
								<li>
									<blockquote>Aliquam aliquet, est a ullamcorper condimentum, tellus nulla fringilla elit, a iaculis nulla turpis sed wisi. Fusce volutpat. Etiam sodales ante id nunc. Proin ornare dignissim lacus. Nunc porttitor nunc a sem. Sed sollicitudin velit eu magna. Aliquam erat volutpat. Vivamus ornare est non wisi. Proin vel quam. Vivamus egestas. Nunc tempor diam vehicula mauris. Nullam sapien eros, facilisis vel, eleifend non, auctor dapibus, pede.</blockquote>
									<cite>- John Doe, <a href="" title="">Company Name</a></cite>
								</li>
								<li>
									<blockquote>Aliquam aliquet, est a ullamcorper condimentum, tellus nulla fringilla elit, a iaculis nulla turpis sed wisi. Fusce volutpat. Etiam sodales ante id nunc. Proin ornare dignissim lacus. Nunc porttitor nunc a sem. Sed sollicitudin velit eu magna. Aliquam erat volutpat. Vivamus ornare est non wisi. Proin vel quam. Vivamus egestas. Nunc tempor diam vehicula mauris. Nullam sapien eros, facilisis vel, eleifend non, auctor dapibus, pede.</blockquote>
									<cite>- John Doe, <a href="" title="">Company Name</a></cite>
								</li>
								<li class="last">
									<blockquote>Aliquam aliquet, est a ullamcorper condimentum, tellus nulla fringilla elit, a iaculis nulla turpis sed wisi. Fusce volutpat. Etiam sodales ante id nunc. Proin ornare dignissim lacus. Nunc porttitor nunc a sem. Sed sollicitudin velit eu magna. Aliquam erat volutpat. Vivamus ornare est non wisi. Proin vel quam. Vivamus egestas. Nunc tempor diam vehicula mauris. Nullam sapien eros, facilisis vel, eleifend non, auctor dapibus, pede.</blockquote>
									<cite>- John Doe, <a href="" title="">Company Name</a></cite>
								</li>
							</ul>
						</div>
					<h2 class="mainTitle"><spring:message code="about.mainblocs.recommanded"/></h2>
						<div class="twoColumnList">
							<ul class="left">
								<li><a href="http://www.adrianpelletier.com" title="Visit this recommended website">Adrian Pelletier</a></li>
								<li><a href="http://www.designkindle.com" title="Visit this recommended website">Design Kindle</a></li>
								<li><a href="http://csstypeset.com" title="Visit this recommended website">CSS Type Set</a></li>
							</ul>
							<ul class="right">
								<li><a href="http://www.buildinteractive.com" title="Visit this recommended website">Build Interactive</a></li>
								<li><a href="http://dribbble.com/players/buildinteractive" title="Visit this recommended website">Dribbble</a></li>
								<li><a href="http://adrianpelletier.com/sandbox/jquery_hover_nav" title="Visit this recommended website">jQuery Hover Nav</a></li>
							</ul>
						</div>
					<h2 class="mainTitle"><spring:message code="about.mainblocs.connect"/></h2>
						<ul class="social">
							<li><a href="#" class="flickr" title="">Link</a></li>
							<li><a href="#" class="myspace" title="">Link</a></li>
							<li><a href="#" class="facebook" title="">Link</a></li>
							<li><a href="#" class="twitter" title="">Link</a></li>
							<li><a href="#" class="delicious" title="">Link</a></li>
							<li><a href="#" class="linkedin" title="">Link</a></li>
							<li><a href="#" class="lastfm" title="">Link</a></li>
							<li><a href="#" class="stumbleupon" title="">Link</a></li>
						</ul>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
