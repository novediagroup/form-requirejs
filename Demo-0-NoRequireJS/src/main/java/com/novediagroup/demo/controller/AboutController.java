package com.novediagroup.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/about")
public class AboutController {

	private static final Logger logger = LoggerFactory
			.getLogger(AboutController.class);

	/**
	 * GET
	 * 
	 * @param model
	 */
	@RequestMapping(method = RequestMethod.GET)
	public void doGet(Model model) {
		logger.info("About - doGet");
	}
}
