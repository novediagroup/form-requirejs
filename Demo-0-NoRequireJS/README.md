#Demo-0-NoRequireJS

Le projet contient toutes les sources de l'application, sans la librairie RequireJS

#### Utilisation

Dans un terminal, se placer à la source du projet puis utiliser la commande suivante :

**mvn jetty:run** (nécessite Maven 3 et JDK 1.6+)

Ouvrir ensuite dans un navigateur la page **http://localhost:8080/**