## RequireJS - Présentation Novedia ##

Vous trouverez sur ce répository Git l'ensemble des projets relatifs à la présentation (format soirée Novedia Shares + BBL) de la librairie RequireJS.

NB : Chaque projet utilise Maven et embarque un serveur Jetty léger vous permettant de lancer rapidement et sans configuration un serveur d'application sur lequel est déployé l'application de démonstration. **Veuillez consulter les fichiers README.md de chaque projet afin d'avoir la commande de lancement à exécuter ainsi que l'ensemble des modifications faites au code pour réaliser l'application de démonstration**.

###Demo-0-NoRequireJS

Le projet contient toutes les sources de l'application, sans la librairie RequireJS.

###Demo-1-RequireJS-SinglePage 

Le projet contient toutes les sources de l'application ainsi qu'un exemple d'utilisation de la librairie RequireJS sur la page *Contact*.

###Demo-2-RequireJS-MultiPage
 
Le projet contient toutes les sources de l'application avec la librairie RequireJS utilisée sur l'ensemble des trois pages du site.

###Demo-3-RequireJS-Plugins
 
Le projet contient toutes les sources de l'application avec RequireJS et ses trois plugins (domReady, text et i18n).

###Demo-4-RequireJS-Optimizer
 
Le projet contient toutes les sources de l'application avec la librairie RequireJS, ses trois plugins et l'utilisation de RequireJS Optimizer à la compilation