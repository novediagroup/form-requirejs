define({
    "root": {
        "validationErrorMsg": "The field '{0}' must be filled !\n",
        "okMessage": "Your message has been successfully sent !",
    },
    "fr": true,
    "it": true
});