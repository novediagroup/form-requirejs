define([], function() {
	return {
		prepareMsg : function(msg) {
			var nbs = unescape("%A0");
			var lines = [0];
			var max = 0;
			var i = 0;
			var last = i;
			for( var j = 1; true; j++ ) {
				last = i;
				i = msg.indexOf('\n',i+1);
			    if( i > 0 ) {
			    	lines[j] = i - last;
			    	if( lines[j] > max )
			    		max = lines[j];
				} else
					break;
			}
			last = msg.length - last;
			if( last > max )
				max = last;
			if( max < 20 )
				max = 20;
			msg = msg.replace(/ /g,nbs);
			for( var j = 1; j < lines.length; j++ )//multiplication because 'spaces' are thinner than other chars
				msg = msg.replace(/\n/,new Array(Math.round((max-lines[j])*1.8)).join(nbs)+' ');
		  
			//fill also the last line so it won't be small enough to displayed together the previous
			return msg += new Array(Math.round((max-last)*1.8)).join(nbs);
		}
	};
});