
require(['jquery', 'easing', 'localscroll', 'cycle', 'tooltipster', 'carousel', 'fancybox', 'domReady!'], function(jQuery, easing, localScroll, cycle, tooltipster, carousel, fancybox) {
	// LocalScroll - "To the Top" links
	$(".toTheTop").localScroll();
    	
    // Cycle - Used for phone slideshow at the top of the page
	$("#slideshow").cycle({
		fx: 'fade',
		speed:  1000,
		timeout:  3500
	});

	// Append shadow image to normal featured work item
	$("#featuredWork").append('<img class="shadow" src="/img/work-shadow-lg.jpg" width="290" height="43" alt="" />');

	// Animate buttons, shrink and fade shadow
	$("#featuredWork").hover(function() {
		var e = this;
	    $(e).find("a").stop().animate({ marginTop: "-14px" }, 250, function() {
	    	$(e).find("a").animate({ marginTop: "-10px" }, 250);
	    });
	    $(e).find("img.shadow").stop().animate({ width: "70%", height: "36px", marginLeft: "50px", opacity: 0.375 }, 250);
	},function(){
		var e = this;
	    $(e).find("a").stop().animate({ marginTop: "4px" }, 250, function() {
	    	$(e).find("a").animate({ marginTop: "0px" }, 250);
	    });
	    $(e).find("img.shadow").stop().animate({ width: "100%", height: "43px", marginLeft: "0", opacity: 1 }, 250);
	});

	// Append shadow image to normal sized work item
	$("#workThumbs span").not("#featuredWork").append('<img class="shadow" src="/img/work-shadow-sm.jpg" width="148" height="43" alt="" />');

	// Animate buttons, shrink and fade shadow
	$("#workThumbs span").not("#featuredWork").hover(function() {
		var e = this;
	    $(e).find("a").stop().animate({ marginTop: "-14px" }, 250, function() {
	    	$(e).find("a").animate({ marginTop: "-10px" }, 250);
	    });
	    $(e).find("img.shadow").stop().animate({ width: "70%", height: "33px", marginLeft: "23px", opacity: 0.375 }, 250);
	},function(){
		var e = this;
	    $(e).find("a").stop().animate({ marginTop: "4px" }, 250, function() {
	    	$(e).find("a").animate({ marginTop: "0px" }, 250);
	    });
	    $(e).find("img.shadow").stop().animate({ width: "100%", height: "43px", marginLeft: "0", opacity: 1 }, 250);
	});
	
	$("#workThumbs").jcarousel({
        // This tells jCarousel NOT to autobuild prev/next buttons
        buttonNextHTML: null,
        buttonPrevHTML: null,
        easing: "easeInOutExpo",
        animation: "slow",
        size: 3,
        scroll: 1,
        initCallback: function jcarousel_initCallback(carousel) {
		    $('#prevWork').bind('click', function() {
		    	carousel.prev();
		        return false;
		    });

		    $('#nextWork').bind('click', function() {
		        carousel.next();
		        return false;
		    });
		},
        buttonPrevCallback: function(carousel, controlElement, state) {
        	if(!state) {
        		$("#prevWork").addClass("disabled");
        	} else {
        		$("#prevWork").removeClass("disabled");
        	}
        },
        buttonNextCallback: function(carousel, controlElement, state) {
        	if(!state) {
        		$("#nextWork").addClass("disabled");
        	} else {
        		$("#nextWork").removeClass("disabled");
        	}
        }
	});
    	
    // FancyBox - Popin Work Thumbnails
    $("#workThumbs a").fancybox({
    	'zoomSpeedIn': 500,
    	'zoomSpeedOut': 500,
    	'overlayShow': true,
    	'easingIn': 'easeOutBack',
    	'easingOut': 'easeInBack'
    });
    
    // ToolTipster => tooltips
	$('.tooltip').tooltipster({
		position: 'bottom'
	});
});

