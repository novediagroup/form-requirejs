<%@ taglib prefix="spring"	uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" 		uri="http://java.sun.com/jstl/core" %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<script type="text/javascript">
			var userLocale = "${pageContext.response.locale}";
		</script>
		
		<title><sitemesh:write property='title'/></title>
		<sitemesh:write property='head'/>
	</head>

	<body>
		<div id="headerWrapper">
			<div id="header">
				<div class="copy">
					<div id="slideshow">
						<img src="<c:url value="/img/slideshow-work-1.jpg"/>" width="505" height="284" alt="" />
						<img src="<c:url value="/img/slideshow-work-2.jpg"/>" width="505" height="284" alt="" />
						<img src="<c:url value="/img/slideshow-work-3.jpg"/>" width="505" height="284" alt="" />
					</div>
					<div id="changeLang" style="float: right; padding-top: 20px; height: 20px;">
	    				<a href="?lang=fr"><img alt="" src="<c:url value="/img/flag_fr.png"/>"></a>
	    				<a href="?lang=en"><img alt="" src="<c:url value="/img/flag_en.png"/>"></a>
	    			</div>
					<h1 id="logo"><spring:message code="common.appdesigner"/></h1>
	    			<ul id="mainNav">
	    				<li><a id="mainNavWork" href="<c:url value="/work"/>" class="tooltip" title="<spring:message code="common.work.desc"/>"><spring:message code="common.work"/></a></li>
	    				<li><a id="mainNavAbout" href="<c:url value="/about"/>" class="tooltip" title="<spring:message code="common.about.desc"/>"><spring:message code="common.about"/></a></li>
	    				<li><a id="mainNavContact" href="<c:url value="/contact"/>" class="tooltip" title="<spring:message code="common.contact.desc"/>"><spring:message code="common.contact"/></a></li>
	    			</ul>
	    		</div>
			</div>
		</div>
	
		<sitemesh:write property='body'/>
		
		<div id="copyright">
			<p><spring:message code="common.copyright"/></p>
			<span class="toTheTop"><a href="#headerWrapper" title="<spring:message code="common.topscroll.desc"/>"><spring:message code="common.topscroll"/></a></span>
		</div>
	</body>
</html>