#Demo-3-RequireJS-Plugins
 
Le projet contient toutes les sources de l'application avec RequireJS et ses trois plugins

####A noter : 
* L'utilisation du plugin **domReady** sur l'ensemble des pages du site,
* L'utilisation du plugin **text** sur la page *About* :
	* 1 - Externalisation du template Handlebars dans le dossier *js/templates*
	* 2 - Chargement du template dans le fichier *js/app/about.js*
* L'utilisation du plugin **i18n** sur la page Contact :
	* Création des fichiers de traduction dans le dossier **nls**
	* La locale est définie côté JavaScript dans le décorateur utilisé sur toutes les pages du site (*WEB-INF/decorators*)
	* La locale est passée dans l'objet de configuration de RequireJS dans le fichier *js/common.js*.

#### Utilisation

Dans un terminal, se placer à la source du projet puis utiliser la commande suivante :

**mvn jetty:run** (nécessite Maven 3 et JDK 1.6+)

Ouvrir ensuite dans un navigateur la page **http://localhost:8080/**