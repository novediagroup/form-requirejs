#Demo-2-RequireJS-MultiPage
 
Le projet contient toutes les sources de l'application avec la librairie RequireJS utilisée sur l'ensemble des trois pages du site.

####A noter : 

* L'ajout du fichier **common.js** à la racine des JavaScript permettant la mutualisation de la configuration entre les différentes pages,
* L'ajout du dossier *js/pages* contenant les JavaScript "passerelles" permettant de charger la configuration avant exécution du code,* La suppression des imports de JavaScript dans toutes les vues (*WEB-INF/views*), seul reste l'import de la librairie RequireJS,
* L'adaptation du code JavaScript en module AMD sur l'ensemble des pages du site.

#### Utilisation

Dans un terminal, se placer à la source du projet puis utiliser la commande suivante :

**mvn jetty:run** (nécessite Maven 3 et JDK 1.6+)

Ouvrir ensuite dans un navigateur la page **http://localhost:8080/**