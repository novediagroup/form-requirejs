requirejs.config({
    paths: {        
        jquery: '../lib/jquery-1.9.1',
        easing: '../lib/jquery.easing.1.3',
        toastmessages_util: '../utils/toastmessages_util',
        toastmessage: '../lib/toastmessages/jquery.toastmessage',
        localscroll: '../lib/jquery.localscroll-1.2.7-min',
        scrollto: '../lib/jquery.scrollTo-min',
        cycle: '../lib/jquery.cycle.min',
        searchField: '../lib/jquery.searchField',
        form: '../lib/jquery.form',
        tooltipster: '../lib/jquery.tooltipster',
        carousel: '../lib/jquery.jcarousel',
        fancybox: '../lib/fancybox/jquery.fancybox'
    },
    shim: {
    	'easing': ['jquery'],
        'toastmessage': ['jquery'],
        'scrollto': ['jquery'],
        'localscroll': ['jquery', 'scrollto'],
        'cycle': ['jquery'],
        'searchField': ['jquery'],
        'form': ['jquery'],
        'tooltipster': ['jquery'],
        'carousel': ['jquery'],
        'fancybox': ['jquery']
    }
});