
require(['jquery', 'easing', 'localscroll', 'cycle', 'tooltipster', 'carousel'], function(jQuery, easing, localScroll, cycle, tooltipster, carousel) {
	$(document).ready(function() {
		// LocalScroll - "To the Top" links
		$(".toTheTop").localScroll();
	    	
	    // Cycle - Used for phone slideshow at the top of the page
		$("#slideshow").cycle({
			fx: 'fade',
			speed:  1000,
			timeout:  3500
		});

		$("#testimonials").jcarousel({
	        // This tells jCarousel NOT to autobuild prev/next buttons
	        buttonNextHTML: null,
	        buttonPrevHTML: null,
	        easing: "easeInOutExpo",
	        animation: "slow",
	        size: 3,
	        scroll: 1,
	        initCallback: function jcarousel_initCallback(carousel) {
			    jQuery('#prevTestimonials').bind('click', function() {
			    	carousel.prev();
			        return false;
			    });

			    jQuery('#nextTestimonials').bind('click', function() {
			        carousel.next();
			        return false;
			    });
			},
	        buttonPrevCallback: function(carousel, controlElement, state) {
	        	if(!state) {
	        		$("#prevTestimonials").addClass("disabled");
	        	} else {
	        		$("#prevTestimonials").removeClass("disabled");
	        	}
	        },
	        buttonNextCallback: function(carousel, controlElement, state) {
	        	if(!state) {
	        		$("#nextTestimonials").addClass("disabled");
	        	} else {
	        		$("#nextTestimonials").removeClass("disabled");
	        	}
	        }
		});

	    // Social links
		$(".social li").hover(function(){
			$(this).find("a").stop().animate({ marginTop: "-5px" }, 200);
		},function(){
			$(this).find("a").stop().animate({ marginTop: "0" }, 200);
		});
		
		// ToolTipster => tooltips
		$('.tooltip').tooltipster({
			position: 'bottom'
		});
	});
});

