#Demo-1-RequireJS-SinglePage 

Le projet contient toutes les sources de l'application ainsi qu'un exemple d'utilisation de la librairie RequireJS sur la page *Contact*.

####A noter : 
* La suppression des imports de JavaScript dans la vue associée à cette page (*WEB-INF/views/contact.jsp*), seul reste l'import de la librairie RequireJS,

* L'adaptation du code JavaScript en module AMD (*js/app/contact.js*).
	
#### Utilisation

Dans un terminal, se placer à la source du projet puis utiliser la commande suivante :

**mvn jetty:run** (nécessite Maven 3 et JDK 1.6+)

Ouvrir ensuite dans un navigateur la page **http://localhost:8080/**