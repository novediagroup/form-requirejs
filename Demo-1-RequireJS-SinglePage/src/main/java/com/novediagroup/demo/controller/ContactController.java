package com.novediagroup.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/contact")
public class ContactController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ContactController.class);

	/**
	 * GET
	 * 
	 * @param model
	 */
	@RequestMapping(method = RequestMethod.GET)
	public void doGet(Model model) {
		LOGGER.info("Contact - doGet");
	}
	
	/**
	 * POST
	 * 
	 * @param model
	 */
	@RequestMapping(method = RequestMethod.POST)
	public void doPost(@ModelAttribute("name") String name,
						@ModelAttribute("e-mail") String email,
						@ModelAttribute("subject") String subject,
						@ModelAttribute("message") String message) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("Contact - doPost").append("\n");
		sb.append("  name = ").append(name).append("\n");
		sb.append("  e-mail = ").append(email).append("\n");
		sb.append("  subject = ").append(subject).append("\n");
		sb.append("  message = ").append(message);
		LOGGER.info(sb.toString());
	}
}
