
require(['jquery', 'toastmessages_util', 'toastmessage', 'localscroll', 'cycle', 'searchField', 'form', 'tooltipster', 'i18n!nls/trads', 'domReady!'], function(jQuery, toastMessagesUtils, toastMessages, localScroll, cycle, searchField, form, tooltipster, trads) {
	// LocalScroll - "To the Top" links
	$(".toTheTop").localScroll();
    	
    // Cycle - Used for phone slideshow at the top of the page
	$("#slideshow").cycle({
		fx: 'fade',
		speed:  1000,
		timeout:  3500
	});
		
	// Clear Form Fields		
	$("input, textarea").searchField();
	
	// Jquery Form
	$('#contactForm').ajaxForm({ 
	    url:  '/contact', 
	    type: 'post',
	    beforeSubmit: function(arr, $form, options) { 
	    	var errorMsg = "";
	    	$.each(arr, function(index, input) {
	    		if(this.name === this.value) {
	    			errorMsg += trads.validationErrorMsg.replace("{0}", this.name);
	    		}
	    	});
	    	if(errorMsg !== "") {
	    		$().toastmessage('showToast', {
		    		text     : toastMessagesUtils.prepareMsg(errorMsg.substring(0, errorMsg.length - 2)),
		    		stayTime : 2000,
		    	    sticky   : false,
		    	    position : 'middle-center',
		    	    type     : 'error'
		    	});
	    		return false;
	    	}
	    },
	    success: function() {
	    	$().toastmessage('showToast', {
	    		text     : trads.okMessage,
	    		stayTime : 1800,
	    	    sticky   : false,
	    	    position : 'middle-center',
	    	    type     : 'success'
	    	});
	    }
	});
	
	// ToolTipster => tooltips
	$('.tooltip').tooltipster({
		position: 'bottom'
	});
});
