
require(['jquery', 'easing', 'localscroll', 'cycle', 'tooltipster', 'carousel', 'handlebars', 'text!templates/testimonial.template', 'domReady!'], function(jQuery, easing, localScroll, cycle, tooltipster, carousel, handlebars, testimonialsTemplate) {
	// LocalScroll - "To the Top" links
	$(".toTheTop").localScroll();
    	
    // Cycle - Used for phone slideshow at the top of the page
	$("#slideshow").cycle({
		fx: 'fade',
		speed:  1000,
		timeout:  3500
	});

	// Testimonials
	$.getJSON('/about/testimonials', function(testimonials) {
		// Let Handlebars populate testimonials
		var template = Handlebars.compile(testimonialsTemplate);
		$('#testimonials').html(template(testimonials));
		
		// Then Carousel it
		$("#testimonials").jcarousel({
	        buttonNextHTML: null,
	        buttonPrevHTML: null,
	        easing: "easeInOutExpo",
	        animation: "slow",
	        size: 3,
	        scroll: 1,
	        initCallback: function jcarousel_initCallback(carousel) {
			    jQuery('#prevTestimonials').bind('click', function() {
			    	carousel.prev();
			        return false;
			    });

			    jQuery('#nextTestimonials').bind('click', function() {
			        carousel.next();
			        return false;
			    });
			},
	        buttonPrevCallback: function(carousel, controlElement, state) {
	        	if(!state) {
	        		$("#prevTestimonials").addClass("disabled");
	        	} else {
	        		$("#prevTestimonials").removeClass("disabled");
	        	}
	        },
	        buttonNextCallback: function(carousel, controlElement, state) {
	        	if(!state) {
	        		$("#nextTestimonials").addClass("disabled");
	        	} else {
	        		$("#nextTestimonials").removeClass("disabled");
	        	}
	        }
		});
	});

    // Social links
	$(".social li").hover(function(){
		$(this).find("a").stop().animate({ marginTop: "-5px" }, 200);
	},function(){
		$(this).find("a").stop().animate({ marginTop: "0" }, 200);
	});
		
	// ToolTipster => tooltips
	$('.tooltip').tooltipster({
		position: 'bottom'
	});
});

