<%@ taglib prefix="spring"	uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" 		uri="http://java.sun.com/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<title><spring:message code="work.title"/></title>

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="<c:url value="/css/screen.css"/>" media="all" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/css/tooltipster.css"/>" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/js/lib/fancybox/jquery.fancybox.css"/>" media="all" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/css/app/work.css"/>" />

		<!-- JavaScript -->
		<script data-main="<c:url value="/js/pages/work.js"/>" src="<c:url value="/js/require.js"/>"></script>
	</head>
	
	<body>
		<div id="content">
			<div class="copy">
				<div id="work" class="contentDivide clearfix">
					<h1 id="workControls" class="mainTitle clearfix">
						<strong><spring:message code="work.title"/></strong>
						<span>
							<a href="javascript:void(0);" id="prevWork"><spring:message code="common.previous"/></a>
							<a href="javascript:void(0);" id="nextWork"><spring:message code="common.next"/></a>
						</span>
					</h1>
					<div>
						<ul id="workThumbs">
							<li>
								<span id="featuredWork"><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/featured.png"/>" width="304" height="292" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-1.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-2.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-3.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-1.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-2.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-3.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-1.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-2.png"/>" width="130" height="130" alt="Project Name" /></a></span>
							</li>
							<li>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-1.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-3.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-1.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-2.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-1.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-2.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-3.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-2.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-1.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-2.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-3.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-1.png"/>" width="130" height="130" alt="Project Name" /></a></span>
							</li>
							<li>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-3.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-2.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-1.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-3.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-1.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-2.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-3.png"/>" width="130" height="130" alt="Project Name" /></a></span>
								<span><a href="<c:url value="/img/work/work-full-1.jpg"/>" title="Project Title Goes Here"><img src="<c:url value="/img/work/work-example-1.png"/>" width="130" height="130" alt="Project Name" /></a></span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
