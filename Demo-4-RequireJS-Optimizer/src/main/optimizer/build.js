{
    appDir: '../webapp',
    baseUrl: 'js',
    dir: '../../../target/optimized-build',
    fileExclusionRegExp: /^\./,
    preserveLicenseComments: false,
    mainConfigFile: '../webapp/js/common.js',
    modules: [
        //First set up the common build layer.
        {
            //module names are relative to baseUrl
            name: 'common',
        },

        //Now set up a build layer for each page, but exclude
        //the common one. "exclude" will exclude nested
        //the nested, built dependencies from "common". Any
        //"exclude" that includes built modules should be
        //listed before the build layer that wants to exclude it.
        //"include" the appropriate "app/main*" module since by default
        //it will not get added to the build since it is loaded by a nested
        //require in the page*.js files.
        {
            //module names are relative to baseUrl/paths config
            name: 'pages/about',
            include: ['app/about'],
            exclude: ['common']
        },

        {
            //module names are relative to baseUrl/paths config
            name: 'pages/contact',
            include: ['app/contact'],
            exclude: ['common']
        },
        
        {
            //module names are relative to baseUrl/paths config
            name: 'pages/work',
            include: ['app/work'],
            exclude: ['common']
        }
    ]
}
