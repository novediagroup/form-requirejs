package com.novediagroup.demo.bean;

import java.util.List;

public class Testimonials {
	private List<Testimonial> testimonials;

	public List<Testimonial> getTestimonials() {
		return testimonials;
	}

	public void setTestimonials(List<Testimonial> testimonials) {
		this.testimonials = testimonials;
	}
}
