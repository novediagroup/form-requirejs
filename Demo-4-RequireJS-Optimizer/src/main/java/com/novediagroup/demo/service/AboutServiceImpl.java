package com.novediagroup.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.novediagroup.demo.bean.Testimonial;
import com.novediagroup.demo.bean.Testimonials;

@Service("aboutService")
public class AboutServiceImpl implements AboutService {

	public Testimonials getTestimonials() {
		Testimonials testimonials = new Testimonials();
		List<Testimonial> testimonialsList = new ArrayList<Testimonial>();
		
		// 1rst
		Testimonial testimonial1 = new Testimonial();
		testimonial1.setAuthor("Reinier Zwitserloot");
		testimonial1.setText("Project Lombok makes java a spicier language by adding 'handlers' that know how to build and compile simple, boilerplate-free, not-quite-java code. See LICENSE for the Project Lombok license.");
		testimonial1.setLinkLabel("Click here !");
		testimonial1.setLinkHref("http://zwitserloot.com/");
		testimonialsList.add(testimonial1);
		
		// 2nd
		Testimonial testimonial2 = new Testimonial();
		testimonial2.setAuthor("James Burke");
		testimonial2.setText("RequireJS is a JavaScript file and module loader. It is optimized for in-browser use, but it can be used in other JavaScript environments, like Rhino and Node. Using a modular script loader like RequireJS will improve the speed and quality of your code.");
		testimonial2.setLinkLabel("Download it !");
		testimonial2.setLinkHref("http://www.requirejs.org/");
		testimonialsList.add(testimonial2);

		// 3rd
		Testimonial testimonial3 = new Testimonial();
		testimonial3.setAuthor("Handlebars team");
		testimonial3.setText("Handlebars provides the power necessary to let you build semantic templates effectively with no frustration. Mustache templates are compatible with Handlebars, so you can take a Mustache template, import it into Handlebars, and start taking advantage of the extra Handlebars features.");
		testimonial3.setLinkLabel("Try it !");
		testimonial3.setLinkHref("http://handlebarsjs.com/");
		testimonialsList.add(testimonial3);
		
		testimonials.setTestimonials(testimonialsList);
		return testimonials;
	}

}
