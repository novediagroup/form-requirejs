package com.novediagroup.demo.service;

import com.novediagroup.demo.bean.Testimonials;

public interface AboutService {
	public Testimonials getTestimonials();
}
