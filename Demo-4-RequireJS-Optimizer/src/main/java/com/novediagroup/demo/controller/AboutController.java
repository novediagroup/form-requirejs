package com.novediagroup.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.novediagroup.demo.bean.Testimonials;
import com.novediagroup.demo.service.AboutService;

@Controller
@RequestMapping("/about")
public class AboutController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AboutController.class);

	@Autowired
	private AboutService aboutService;
	
	/**
	 * GET
	 * 
	 * @param model
	 */
	@RequestMapping(method = RequestMethod.GET)
	public void doGet(Model model) {
		LOGGER.info("About - doGet");
	}
	
	@RequestMapping("testimonials")
	@ResponseBody
	public Testimonials getTestimonials() {
		LOGGER.info("About - getTestimonials");
		return aboutService.getTestimonials();
	}
}
