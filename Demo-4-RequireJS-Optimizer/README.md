#Demo-4-RequireJS-Optimizer
 
Le projet contient toutes les sources de l'application avec la librairie RequireJS, ses trois plugins et l'utilisation de RequireJS Optimizer à la compilation

####A noter : 
* La création du dossier src/main/optimizer contenant deux fichiers :
	* **build.js**, contenant les directives que l'on souhaite voir appliquer par RequireJS Optimizer,
	* **r.js**, le script d'optimisation fourni par RequireJS.

* La modification du fichier **pom.xml** : 
	* Déclaration et configuration du plugin **requirejs-maven-plugin** optimisant les fichiers JS et CSS à la compilation du projet dans le dossier *target/optimized-build*,
	* Déclaration et configuration du plugin **maven-war-plugin** permettant de rapatrier les fichiers optimisés dans le WAR final.

#### Utilisation

Dans un terminal, se placer à la source du projet puis utiliser la commande suivante :

**mvn jetty:run-war**
ou
**mvn jetty:run-exploded**

(nécessite Maven 3 et JDK 1.6+)

Ouvrir ensuite dans un navigateur la page **http://localhost:8080/**

*NB: Il est nécessaire de démarrer sur les sources compilées par RequireJS Optimizer, d'où l'obligation de passer par un WAR*

